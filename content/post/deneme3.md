---
title: Deneme post
date: 2022-12-17
draft: false
---



Orijinal adı ile “Relatos Salvajes”

İngilizce adı ile “Wild Tales”

Vizyonumuzdaki ismi ile “Asabiyim Ben”


Türü / Süresi: Komedi / Drama / Gerilim / 122 Dakika

Yönetmeni: Damian SZİFRON

Senaristi: Damian SZİFRON

Oyuncusu: Dario GRANDİNETTİ, Ricardo DARİN, Rita CORTESE,
Julieta ZYLBERBERG, Erica RİVAS


IMDB Puanı: 8.2 / 10

Filmimiz altı adet kısa ve birbirinden bağımsız filmden oluşuyor. Hangisi daha etkileyiciydi, hala karar verebilmiş değilim.